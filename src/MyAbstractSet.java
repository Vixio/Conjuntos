import java.util.AbstractSet;

/**
 * Created by christian on 05/11/15.
 */
public abstract class MyAbstractSet<T> extends AbstractSet<T> {

    class Pair<T, K> {
        private T val1;
        private K val2;

        public Pair(T val1, K val2) {
            this.val1 = val1;
            this.val2 = val2;
        }

        public T getVal1() {
            return val1;
        }

        public K getVal2() {
            return val2;
        }

        @Override
        public String toString() {
            return "(" + val1 + ", " + val2 + ")";
        }
    }

    abstract boolean isIn(T item);

    abstract MyAbstractSet<T> union(MyAbstractSet<T> coll);

    abstract MyAbstractSet<T> intersection(MyAbstractSet<T> coll);

    abstract MyAbstractSet<T> difference(MyAbstractSet<T> coll);

    abstract MyAbstractSet<T> complement(MyAbstractSet<T> universe);

    abstract <K> MyAbstractSet<Pair<T, K>> productC(MyAbstractSet<K> set);

    abstract boolean subset(MyAbstractSet<T> set);

    abstract boolean subsetP(MyAbstractSet<T> set);

    abstract MyAbstractSet<T> empty();
}
