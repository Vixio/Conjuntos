import java.util.*;

/**
 * Created by christian on 15/10/15.
 */
public class MySet<T> extends MyAbstractSet<T> implements Cloneable {

    private HashSet<T> collection;

    public MySet() {
        this(new HashSet<T>());
    }

    public MySet(Collection<T> collection) {
        this.collection = new HashSet<>(collection);
    }

    @Override
    public int size() {
        return collection.size();
    }

    @Override
    public Iterator<T> iterator() {
        return collection.iterator();
    }

    @Override
    public boolean add(T t) {
        return collection.add(t);
    }

    @Override
    public String toString() {
        if ( collection.size() == 0 ) {
            return "{}";
        }

        String elems = collection.toString();

        return "{" + elems.substring(1, elems.length() - 1) + "}";
    }

    @Override
    public boolean equals(Object o) {
        if ( this == o ) {
            return true;
        }

        if ( o == null || getClass() != o.getClass() ) {
            return false;
        }

        MySet<?> mySet = (MySet<?>) o;

        return collection.equals( mySet.collection );
    }


    @Override
    public Object clone() throws CloneNotSupportedException {
        MySet<T> obj = new MySet<>(this.collection);

        return obj;
    }

    @Override
    public boolean isIn(T item) {
        return contains(item);
    }

    @Override
    public MyAbstractSet<T> union(MyAbstractSet<T> coll) {
        MySet<T> res = new MySet<>(this.collection);

        res.addAll(coll);

        return res;
    }

    @Override
    public MyAbstractSet<T> intersection(MyAbstractSet<T> coll) {
        MySet<T> res = new MySet<>(this.collection);

        res.retainAll(coll);

        return res;
    }

    @Override
    public MyAbstractSet<T> difference(MyAbstractSet<T> coll) {
        MySet<T> res = new MySet<>(this.collection);

        res.removeAll(coll);

        return res;
    }

    @Override
    public MyAbstractSet<T> complement(MyAbstractSet<T> universe) {
        MySet<T> res = new MySet<>(universe);

        res.removeAll(this.collection);

        return res;
    }

    @Override
    public <K> MyAbstractSet<Pair<T, K>> productC(MyAbstractSet<K> set) {

        MyAbstractSet<Pair<T, K>> obj = new MySet<>();

        for ( T o : collection ) {
            for ( K o2 : set ) {
                obj.add(new Pair<>(o, o2));
            }
        }

        return obj;
    }

    @Override
    public boolean subset(MyAbstractSet<T> set) {
        return this.collection.containsAll(set);
    }

    @Override
    public boolean subsetP(MyAbstractSet<T> set) {
        return this.collection.containsAll(set) && this.size() != set.size();
    }

    @Override
    public MyAbstractSet<T> empty() {
        return new MySet<T>();
    }
}
