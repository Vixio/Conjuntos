import java.util.ArrayList;

/**
 * Created by christian on 15/10/15.
 */
public class Main {
    public static void main(String args[]) {
        MySet<Integer> universe = new MySet<>();
        for ( int i = 0; i < 10; i++ ) {
            universe.add(i);
        }

        ArrayList<Integer> setItems1 = new ArrayList<>();
        setItems1.add(1);
        setItems1.add(3);
        setItems1.add(5);
        setItems1.add(6);

        ArrayList<Integer> setItems2 = new ArrayList<>();
        setItems2.add(1);
        setItems2.add(3);
        setItems2.add(9);

        ArrayList<Integer> setItems3 = new ArrayList<>();
        setItems3.add(1);
        setItems3.add(6);

        MySet<Integer> set1 = new MySet<>(setItems1);
        MySet<Integer> set2 = new MySet<>(setItems2);
        MySet<Integer> set3 = new MySet<>(setItems3);
        MySet<Integer> set4 = new MySet<>(setItems2);

        System.out.println("Universo: " + universe);
        System.out.println("Conjunto 1: " + set1);
        System.out.println("Conjunto 2: " + set2);
        System.out.println("Conjunto 3: " + set3);
        System.out.println("Conjunto 4: " + set4);

        System.out.println("-----------------------------");
        System.out.println(" Igualdad ");
        System.out.println("-----------------------------");
        System.out.println("Son igules el conjunto " + set2 + " y " + set4 + ": " + set2.equals(set4));
        System.out.println("Son igules el conjunto " + set1 + " y " + set2 + ": " + set1.equals(set2));

        System.out.println("-----------------------------");
        System.out.println(" Vacio ");
        System.out.println("-----------------------------");
        System.out.println("Conjunto vacio: " + set1.empty());

        System.out.println("-----------------------------");
        System.out.println(" isIn ");
        System.out.println("-----------------------------");
        System.out.println("El elemento 0 existe en el conjunto " + set1 + ": " + set1.isIn(0));
        System.out.println("El elemento 5 existe en el conjunto " + set1 + ": " + set1.isIn(5));

        System.out.println("-----------------------------");
        System.out.println(" Subconjunto ");
        System.out.println("-----------------------------");
        System.out.println("Es " + set3 + " subconjunto de " + set1 +  " : " + set1.subset(set3));
        System.out.println("Es " + set4 + " subconjunto de " + set1 +  " : " + set1.subset(set4));

        System.out.println("-----------------------------");
        System.out.println(" Subconjunto Propio ");
        System.out.println("-----------------------------");
        System.out.println("Es " + set2 + " subconjunto de " + set4 +  " : " + set2.subsetP(set4));
        System.out.println("Es " + set3 + " subconjunto de " + set1 +  " : " + set1.subset(set3));

        System.out.println("-----------------------------");
        System.out.println(" Union");
        System.out.println("-----------------------------");
        System.out.println("Union " + set4 + " de " + set1 +  " : " + set4.union(set1));
        System.out.println("Union " + set2 + " de " + set3 +  " : " + set2.union(set3));

        System.out.println("-----------------------------");
        System.out.println(" Interseccion");
        System.out.println("-----------------------------");
        System.out.println("Interseccion " + set4 + " de " + set1 +  " : " + set4.intersection(set1));
        System.out.println("Interseccion " + set2 + " de " + set3 +  " : " + set2.intersection(set3));

        System.out.println("-----------------------------");
        System.out.println(" Diferencia");
        System.out.println("-----------------------------");
        System.out.println("Interseccion " + set4 + " de " + set1 +  " : " + set4.difference(set1));
        System.out.println("Interseccion " + set2 + " de " + set3 +  " : " + set2.difference(set3));

        System.out.println("-----------------------------");
        System.out.println(" Complemento");
        System.out.println("-----------------------------");
        System.out.println("Universo: " + universe);
        System.out.println("Complemento de " + set4 + ": " + set4.complement(universe));
        System.out.println("Complemento de " + set1 + ": " + set1.complement(universe));

        System.out.println("-----------------------------");
        System.out.println(" Producto Cartesiano");
        System.out.println("-----------------------------");
        System.out.println("Interseccion " + set4 + " de " + set1 +  " : " + set4.productC(set1));
        System.out.println("Interseccion " + set2 + " de " + set3 +  " : " + set2.productC(set3));
    }
}
